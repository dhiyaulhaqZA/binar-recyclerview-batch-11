package id.logivity.binarrecyclerview

data class Tweet(val username: String,
                 val userId: String,
                 val date: String,
                 val text: String)