package id.logivity.binarrecyclerview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var tweetAdapter: TweetAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupComponent()
        addDummyData()
    }

    private fun setupComponent() {
        tweetAdapter = TweetAdapter()

        rv_main_tweet.setHasFixedSize(true)
        rv_main_tweet.layoutManager = LinearLayoutManager(this)
        rv_main_tweet.adapter = tweetAdapter
    }

    private fun addDummyData() {
        val tweetList = mutableListOf<Tweet>()
        for (i in 0..10) {
            tweetList.add(Tweet("paay", "@paay", "3m", "Selamat malam para netijen twitter"))
        }

        tweetAdapter.addTweetList(tweetList)
    }
}
